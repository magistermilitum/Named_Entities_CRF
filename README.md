### Automatic Named entities recognition for latin and french medieval texts

This is an automatic model to entails automatic entity named detections and recognition on medieval charters and chronics. 

Model for latin was trained on a 5 thousands-items database of latin Burgundy documentation. Model for french was trained using 2M words from medieval traveler chronics. 

Models have a double core working the one on personal names and the other on place names. Overlapped entities recognition can be entailed setting CRF output. Institutional entity names is not implemented on this model. 

### Conditional Random Fields approach

This model was trained using a conditional random fields (CRF) approach working in the a statistical linear chain technique. CRF operates predicting sequence labels to sequence of observed tokens using differents hyperparameters: in our case a lbgs-algorithm and a L1:0.2 and L2:0.05

Automatic tagging output can be downloaded in several formats as xml,txt, csv and json. In order to build a developpement enviromment you must download Wapiti CNRS package from [here](https://wapiti.limsi.fr/#download)

Tagging Order must follow the pattern: 
```
wapiti label -m ~/model.txt -c ~/input.txt ~/output.txt
```

A python wrapper for Wapiti can be installed in pip from [here](https://pypi.org/project/libwapiti/)

### Online NER App

You can find deployed on heroku an automatic ner tagger using model developped with this architecture on [here](https://ner-latin.herokuapp.com/)

This online version accepts raw text a post an enriched output with lemma, pos and ner features and a interface destinated to validate automatic hypothesis to facilite transforme from silver-corpora to gold-corpora.